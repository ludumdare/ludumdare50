extends Resource
class_name GameArea

export var area_name := ""
export var area_difficulty := 0
export (Array, PackedScene) var area_sections
export (PackedScene) var area_start_section
export (PackedScene) var area_end_section
export var area_faith := 25
export var is_heaven := false
