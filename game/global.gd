extends Node2D

# adjustable variables

export(Resource) var game_world

export var max_light := 110
export var show_area_guides := true
export var show_variables := true
export var show_colliders := true
export var starting_area := 0

# gameplay variables

var highest_faith := 100
var faith = 0
var light = max_light
var faith_needed := 0
var world : GameWorld
var world_index := -1
var world_area : GameArea

# debug variables

var debug_show_area_guides
var debug_show_variables
var debug_show_colliders
var debug_staring_area

# signals

signal player_dead
signal player_entered(player)
signal game_starting
signal game_ending
signal section_entered
signal pickup_faith(pickup)
signal pickup_sin(pickup)
signal hit_enemy(enemy)
signal area_completed
signal pickup_light


func _ready():
	
	debug_show_area_guides = show_area_guides
	debug_show_variables = show_variables
	debug_show_colliders = show_colliders
	debug_staring_area = starting_area
	world = game_world

func advance_world_area():
	
	world_index += 1 
	world_area = world.game_areas[world_index]
