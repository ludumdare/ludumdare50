extends Node2D

export var settings_config : String = "res://game.cfg"
export var dreamlo_config : String = "res://dreamlo.cfg"
export(Resource) var window_manager

func _ready():

	Ludum.start(settings_config, LudumSettingsResource.new())
	DreamLo.load_settings(dreamlo_config)
	WindowManager.load(window_manager)

	Core.change_state(Ludum.GAME_STATE_INIT)
	
	MusicManager.play("home")
