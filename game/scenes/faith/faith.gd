class_name Faith
extends Area2D

enum FaithType {
	
	FAITH,
	SIN,
}

export(FaithType) var faith_type


func _ready():
	pass


func _on_Faith_area_entered(area):
	Global.emit_signal("pickup_faith", self)
