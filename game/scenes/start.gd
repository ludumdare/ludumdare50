extends Node2D

func _ready():
	
	Global.faith = 0
	Global.faith_needed = 0
	Global.world_index = -1
	Global.light = Global.max_light
	
	Global.advance_world_area()
	
	DreamLo.connect("dreamlo_got_scores", self, "_on_scores_loaded")
	DreamLo.get_scores()
	
	ECS.clean()
	ECS.update()



func _on_scores_loaded(scores):
	
	if (scores):
		if (scores is Array):
			Global.faith_needed = int(scores[0]["score"])
		else:
			Global.faith_needed = int(scores["score"])
	else:
		Global.faith_needed = 0
	
	SceneManager.transition_to("res://game/scenes/ready.tscn")
