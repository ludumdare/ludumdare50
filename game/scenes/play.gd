extends Node2D

onready var camera_holder = $ViewportContainer/Viewport/CanvasModulate/CameraHolder
onready var root = $ViewportContainer/Viewport/CanvasModulate/Root
onready var spawn_controller = $ViewportContainer/Viewport/CanvasModulate/SpawnController as SpawnController
onready var light_controller = $ViewportContainer/Viewport/CanvasModulate/LightController as LightController

export(Resource) var game_area
export var test_game_area := false
export var light_cycles := 5


enum PlayStates {
	
	PLAY_STATE_NONE,
	PLAY_STATE_IDLE,
	PLAY_STATE_INIT,
	PLAY_STATE_RUN,
	PLAY_STATE_STOP,
	PLAY_STATE_WIN,
	PLAY_STATE_LOSE,
	PLAY_STATE_NEXT,
	PLAY_STATE_RESTART,
	PLAY_STATE_HOME,
	PLAY_STATE_PAUSE,
	PLAY_STATE_RESUME,
	PLAY_STATE_EXIT
}

var area : GameArea
var faith_needed : int
var last_section := false
var faith := 0
var last_section_done := false
var play_state = PlayStates.PLAY_STATE_NONE
var next_state = PlayStates.PLAY_STATE_NONE
var state_changed := false
var next_scene := ""
var light_cycle := 0
var is_paused := false
var max_light_cycles := 0

# player object
var player : PlayerEntity

# index of next section to add
var section := 0 

# height of each section in pixels
var section_size := 128 + 16

# starting location of sections
var section_start := 256 + 64


func _ready():
	Logger.trace("[Play] _ready")
	
	Global.connect("player_dead", self, "_on_player_dead")
	Global.connect("player_entered", self, "_on_player_entered")
	Global.connect("section_entered", self, "_on_section_entered")
	Global.connect("pickup_faith", self, "_on_pickup_faith")
	Global.connect("pickup_sin", self, "_on_pickup_sin")
	Global.connect("area_completed", self, "_on_area_completed")
	Global.connect("pickup_light", self, "_on_pickup_light")
	
	MusicManager.play("play")
	
	$CanvasLayer/INFO/Debug.text = ""
	
	if (Global.debug_show_colliders):
		get_tree().debug_collisions_hint = true
		
	if (game_area and test_game_area):
		area = game_area
	else:
		area = Global.world_area
	
	# calculate faith needed for next area
	
	var _fac : float = float(area.area_faith) / 100
	faith_needed = Global.faith_needed * _fac 
	
	
	var _start = area.area_start_section.instance()
	root.add_child(_start)
	_start.global_position = Vector2.ZERO
	
	# spawn anything from the starting section
	
	spawn_controller.load(_start)
	
	# add two sections from pool to start

	_add_section()	
	
	$CanvasLayer/PAUSE.hide()
	
	max_light_cycles = light_cycles
	
	

func _add_section():
	Logger.trace("[Play] _add_section")
	
	if (last_section_done):
		return
	
	if (! last_section):
	
		var _rnd_section = randi() % area.area_sections.size()
		var _section = area.area_sections[_rnd_section].instance()
		root.add_child(_section)
		_section.global_position = Vector2.UP * (section_start + (section * section_size))
		spawn_controller.load(_section)

		section += 1

	else:
		
		var _section = area.area_end_section.instance()
		root.add_child(_section)
		_section.global_position = Vector2.UP * (section_start + ((section + 1) * section_size)) 
		spawn_controller.load(_section)
		last_section_done = true


func _init():
	Logger.trace("[Play] _init")
	

func _on_player_entered(player):
	camera_holder.follow = player
	light_controller.location = player
	self.player = player
	
		
func _on_player_dead():
	Logger.trace("[Play] _on_player_dead")
	change_state(PlayStates.PLAY_STATE_STOP)	
	
	camera_holder.follow = null
	
	if (Global.faith > Global.faith_needed):
		next_scene = "res://game/scenes/win.tscn"
	else:
		next_scene = "res://game/scenes/lose.tscn"


func _on_section_entered():
	Logger.trace("[Play] _on_section_entered")
	call_deferred("_add_section")


func _on_hit_enemy(enemy):
	Logger.trace("[Play] _on_hit_enemy")
	pass
	
	
func _on_pickup_faith(pickup : Faith):
	Logger.trace("[Play] _on_pickup_faith")
	
	if pickup.faith_type == 0:
		faith += 1
		Global.faith += 1
		SoundManager.play("faith")
		
	if pickup.faith_type == 1:
		
		if Global.faith > 0:
			Global.faith -= 1
			faith -= 1
			
		if faith < 0:
			faith = 0
				
		SoundManager.play("sin")
		
	
	#  trigger last section unless we are on heaven level
	if (faith >= faith_needed and ! area.is_heaven):
		last_section = true
		
	pickup.queue_free()
		
	
func _on_pickup_sin(pickup):
	Logger.trace("[Play] _on_pickup_sin")

	faith -= 1 
	Global.faith -= 1
		
	pickup.queue_free()


func _on_area_completed():
	Logger.trace("[Play] _on_area_completed")
	change_state(PlayStates.PLAY_STATE_NEXT)
	

func _process(delta):
	
	if area.is_heaven and (Global.faith > Global.faith_needed):
		max_light_cycles = light_cycles / 2
		
	match play_state:
		
		PlayStates.PLAY_STATE_NONE:
			change_state(PlayStates.PLAY_STATE_INIT)
		
		PlayStates.PLAY_STATE_INIT:
			state_init()
			
		PlayStates.PLAY_STATE_RUN:
			state_run()
			
		PlayStates.PLAY_STATE_NEXT:
			state_next()
			
		PlayStates.PLAY_STATE_HOME:
			state_home()
			
		PlayStates.PLAY_STATE_STOP:
			state_stop()
			
		PlayStates.PLAY_STATE_EXIT:
			state_exit()
			
		PlayStates.PLAY_STATE_PAUSE:
			state_pause()

		PlayStates.PLAY_STATE_RESUME:
			state_resume()

	if (Input.is_action_just_pressed("ui_cancel")):
		
		is_paused = !is_paused
		
		if (!is_paused):
			change_state(PlayStates.PLAY_STATE_RESUME)
		else:
			change_state(PlayStates.PLAY_STATE_PAUSE)
			
	
	if (Global.debug_show_variables):
		_debug()	
		
	if (state_changed):
		state_changed = false
		play_state = next_state
		
func _light_cycle():
	
	if ! player:
		return
	
	light_cycle += 1
	
	if light_cycle > max_light_cycles:
		Global.light -= 1
		light_cycle = 0
		
	var _scale : float = (Global.light) / 100.0
	player.light.texture_scale = _scale
	
	if Global.light <= 10:
		Global.emit_signal("player_dead")
		
	
func state_init():
	change_state(PlayStates.PLAY_STATE_RUN)
	
	
func state_run():
	_light_cycle()
	$CanvasLayer/UI/Faith.text = "%s" % Global.faith
	$CanvasLayer/UI/Needed.text = "%s" % (Global.faith_needed + 1)
	$CanvasLayer/UI/Light.value = Global.light - 10
	ECS.update()
	
	
func state_home():
	next_scene = Ludum.settings.home_scene
	change_state(PlayStates.PLAY_STATE_STOP)

	
func state_pause():
	$CanvasLayer/PAUSE.show()
	change_state(PlayStates.PLAY_STATE_IDLE)	

	
func state_next():
	Global.advance_world_area()
	next_scene = "res://game/scenes/ready.tscn"
	change_state(PlayStates.PLAY_STATE_STOP)
	
	
func state_stop():
	ECS.clean()
	ECS.update()
	change_state(PlayStates.PLAY_STATE_EXIT)
	

func state_exit():
	SceneManager.transition_to(next_scene)
	change_state(PlayStates.PLAY_STATE_NONE)
	
		
func change_state(state):
	
	state_changed = true
	next_state = state
	
	
func state_resume():
	$CanvasLayer/PAUSE.hide()
	change_state(PlayStates.PLAY_STATE_RUN)

		
func _debug():
	
	var _s = ""
	
	_s += _add_var("\n--- local vars ---\n")
	_s += _add_var("state", play_state)
	_s += _add_var("faith", faith)
	_s += _add_var("faith_needed", faith_needed)
	_s += _add_var("area name", area.area_name)
	_s += _add_var("area is heaven", area.is_heaven)
	_s += _add_var("sections", section)
	_s += _add_var("light_cycles", light_cycles)
	_s += _add_var("max_light_cycles", max_light_cycles)
	_s += _add_var("\n--- global vars ---\n")
	_s += _add_var("light", Global.light)
	_s += _add_var("faith", Global.faith)
	_s += _add_var("area index", Global.world_index)
	
	$CanvasLayer/INFO/Debug.text = _s


func _add_var(name, value = null):
	
	if (value != null):
		return "%s = %s \n" % [name, value]
		
	
	return "%s\n" % name	


func _on_Home_pressed():
	change_state(PlayStates.PLAY_STATE_HOME)


func _on_Resume_pressed():
	change_state(PlayStates.PLAY_STATE_RESUME)


func _on_pickup_light():
	Global.light += 3
	SoundManager.play("light")
	
	if (Global.light > Global.max_light):
		Global.light = Global.max_light
