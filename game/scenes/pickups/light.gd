extends Area2D

export var speed := 10

var _dir = Vector2.UP

func _ready():
	 pass
	
	
func _process(delta):
	position += _dir * speed * delta


func _on_Timer_timeout():
	queue_free()


func _on_Light_area_entered(area):
	Global.emit_signal("pickup_light")
	queue_free()
