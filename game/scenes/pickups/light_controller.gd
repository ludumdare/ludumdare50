class_name LightController
extends Node2D

export(PackedScene) var light 
export var emitter_timer := 10.0 # in seconds
export var emitter_range := 1000
export(NodePath) var emitter_location


var location

func _ready():
	
	location = get_node(emitter_location)
	_start_timer()


func _start_timer():
	
	$Timer.wait_time = emitter_timer
	$Timer.start()
	

func _on_Timer_timeout():
	
	if location != null:
	
		var _light = light.instance()
		add_child(_light)
		var _x = rand_range(emitter_range * -1, emitter_range)
		var _y = rand_range(emitter_range * -1, emitter_range)
		var _pos = Vector2(_x, _y) + Vector2(50, 50)
#		_light.global_position = location.global_position
		_light.global_position = location.global_position + _pos
			
	_start_timer()
