extends Node2D

onready var guide = $SectionGuide

func _ready():
	guide.hide()


func _on_Area2D_area_entered(area):
	Global.emit_signal("section_entered")
	get_node("Area2D/CollisionShape2D").set_deferred("disabled", true)
