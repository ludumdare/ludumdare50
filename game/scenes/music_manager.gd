extends Node2D

var current_name : String
var current_player : AudioStreamPlayer

func play(name):
	if (current_player != null):
		current_player.stop()
		
	current_name = name
	current_player = get_node(name) as AudioStreamPlayer
	current_player.play()
	
	
func stop():
	current_player.stop()
	
