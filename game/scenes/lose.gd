extends Node2D

func _ready():
	MusicManager.play("lose")
	$CanvasLayer/Retry.grab_focus()


func _on_Retry_pressed():
	SceneManager.transition_to("res://game/scenes/start.tscn")


func _on_Home_pressed():
	SceneManager.transition_to("res://game/scenes/home.tscn")
