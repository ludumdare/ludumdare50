class_name SpawnController
extends Node2D

func _init():
	pass
	
	
func _ready():
	pass


func load(node: Node):
	
	# find all spawner nodes on the path
	
	for _node in node.get_children():
	
	# add the object from the spawner and destroy the spawner
	
		if (_node.get_class() == "Spawner"):
			
			var _spawner = _node as GameSpawner
			var _index = randi() % _spawner.spawn_scenes.size()
			var _scene = _spawner.spawn_scenes[_index] as PackedScene
			var _new = _scene.instance()
			add_child(_new)
			_new.global_position = _spawner.global_position
			
			_node.queue_free()
	
	pass
	
	
