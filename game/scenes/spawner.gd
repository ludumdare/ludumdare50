class_name GameSpawner
extends Node2D

enum SpawnTypes {
	
	SPAWN_NONE
	SPAWN_LIGHT
	SPAWN_FAITH
	SPAWN_ENEMY
	SPAWN_PLAYER

}

export (SpawnTypes) var spawn_type := SpawnTypes.SPAWN_NONE
export (Array, PackedScene) var spawn_scenes

func get_class(): return "Spawner"
