extends LudumScene

onready var settings = Ludum.settings as LudumSettingsResource

func _ready():
	
	if (! settings):
		return
	
	$Node/VBoxContainer/OptionsButton.visible = settings.show_options
	$Node/VBoxContainer/AboutButton.visible = settings.show_about
	$Node/VBoxContainer/HelpButton.visible = settings.show_help
	$Node/VBoxContainer/ScoresButton.visible = settings.show_scores
	$Node/VBoxContainer/QuitButton.visible = settings.show_quit
	
	$Node/VBoxContainer/PlayButton.grab_focus()
	
	._ready()

	DreamLo.connect("dreamlo_got_scores", self, "_on_scores_loaded")
	DreamLo.get_scores()



func _on_scores_loaded(scores):
	
	var _faith = 0
	
	if (scores):
		if (scores is Array):
			_faith = int(scores[0]["score"])
		else:
			_faith = int(scores["score"])
	else:
		_faith = 0

	$Node/Faith.text = "%s" % (_faith + 1)


func _on_Timer_timeout():
	DreamLo.get_scores()
