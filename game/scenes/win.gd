extends Node2D

func _ready():

	$CanvasLayer/Faith.text = "%s" % Global.faith
	MusicManager.play("win")
	$LineEdit.grab_focus()

func _on_Button_pressed():
	
	DreamLo.add_score_with_message($LineEdit.text, Global.faith, 0, "LD50")
	Core.change_state(Ludum.GAME_STATE_HOME)
	SceneManager.transition_to("res://game/scenes/home.tscn")
