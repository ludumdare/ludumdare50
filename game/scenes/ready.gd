extends Node2D

func _ready():
	MusicManager.play("ready")
	
func _process(delta):
	$CanvasLayer/Label.text = "GET READY\n\nAPPROACHING\n\n%s" % Global.world_area.area_name 
	$CanvasLayer/Time.text = "%s" % int($Timer.time_left)
	
	
func _input(event):
	
	if event is InputEventKey and event.is_pressed():
		_continue()

func _continue():
		SceneManager.transition_to("res://game/scenes/play.tscn")
	


func _on_Timer_timeout():
	_continue()
