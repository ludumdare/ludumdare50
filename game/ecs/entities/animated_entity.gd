class_name AnimatedEntity
extends Entity

onready var animated_sprite = $AnimatedSprite


func _on_Body_area_entered(area):
	Logger.fine("%s hit %s of %s" % [ name, area.name, area.get_parent().name])
	
