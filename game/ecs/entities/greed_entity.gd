class_name GreedEntity
extends AnimatedEntity

export var scan_cycles := 180
export var alert_cycles := 90
export var attack_cycles := 360

enum States {
	
	GREED_STATE_NONE,
	GREED_STATE_LEFT,
	GREED_STATE_RIGHT,
	GREED_STATE_ATTACK,
	GREED_STATE_ALERT,
	GREED_STATE_SCAN
}


var look_left := false
var cycles := 0
var state = States.GREED_STATE_LEFT
var animate : AnimateComponent
var motion : MotionComponent

func get_class(): return "Enemy"


func _ready():
	animate = get_component("animate")
	motion = get_component("motion")
	
func _process(delta):
	
	match state:
		
		States.GREED_STATE_NONE:
			_state_none()
			
		States.GREED_STATE_LEFT:
			_state_left()
			
		States.GREED_STATE_RIGHT:
			_state_right()
			
		States.GREED_STATE_SCAN:
			_state_scan()
			
		States.GREED_STATE_ALERT:
			_state_alert()
			
		States.GREED_STATE_ATTACK:
			_state_attack()
	
	
func _state_none():
	state = States.GREED_STATE_LEFT
	

func _state_attack():
	cycles += 1
	
	if look_left:
		motion.motion.x = -1 * motion.SPEED
		$AnimatedSprite.flip_h = true
	else:
		motion.motion.x = 1 * motion.SPEED
		$AnimatedSprite.flip_h = false
				
	if (cycles > scan_cycles):
		cycles = 0
		motion.motion.x = 0
		animate.animation = "scan_left"
		$AnimatedSprite.flip_h = false
		state = States.GREED_STATE_SCAN
	
		
func _state_alert():
	cycles += 1
	if (cycles > scan_cycles):
		cycles = 0
		animate.animation = "attack"
		state = States.GREED_STATE_ATTACK
	

func _state_scan():
	cycles += 1
	if (cycles > scan_cycles):
		cycles = 0
		if (look_left):
			state = States.GREED_STATE_RIGHT
		else:
			state = States.GREED_STATE_LEFT
			
		
func _state_left():
	animate.animation = "scan_left"
	look_left = true
	$Search.rotation_degrees = -180
	state = States.GREED_STATE_SCAN
		
func _state_right():
	animate.animation = "scan_right"
	look_left = false
	$Search.rotation_degrees = 0
	state = States.GREED_STATE_SCAN
	
	
func _on_Search_area_entered(area):
	Logger.fine("%s hit %s of %s(%s)" % [ name, area.name, area.get_parent().name, area.get_parent().get_class()])

	var _animate = get_component("animate") as AnimateComponent
	_animate.animation = "alert"
	state = States.GREED_STATE_ALERT
