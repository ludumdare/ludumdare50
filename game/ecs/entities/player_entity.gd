class_name PlayerEntity
extends AnimatedEntity

onready var light = $Light2D

func on_ready():
	Global.emit_signal("player_entered", self)


func on_before_remove():
	Global.emit_signal("player_dead")
	
	
func bounce(size):
	Logger.trace("[PlayerEntity] bounce")	
	Logger.debug("bounce")
	SoundManager.play("bounce")
	var _motion = get_component("motion") as MotionComponent
	var _animate = get_component("animate") as AnimateComponent
	var _bounce = size
	
	Logger.debug("%s" % _animate.flip)
	if (_animate.flip):
		_bounce = _bounce * -1
			
	print(_bounce)
				
	_motion.motion.y = -300
	_motion.bounce = _bounce
	_motion.bouncing = false

	_animate.animation = "fall"


func _on_Body_area_entered(area):
	Logger.debug("%s hit %s of %s(%s)" % [ name, area.name, area.get_parent().name, area.get_parent().get_class()])
	
	if (area.name == "Body" and area.get_parent().get_class() == "Enemy"):
		bounce(125)
		return
		
	if (area.name.find("Fire") >= 0):
		bounce(55)
		return
		
	var _collide = get_component("collide") as CollideComponent
	
	if (_collide != null):
		_collide.has_collided = true
		_collide.collision_area = area
