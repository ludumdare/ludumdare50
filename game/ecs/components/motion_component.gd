class_name MotionComponent
extends Component

export var GRAVITY : float = 1000
export var SPEED : float = 100

var motion := Vector2.ZERO
var bounce := 0
var bounce_count := 0
var bouncing := false
