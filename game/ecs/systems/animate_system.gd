class_name AnimateSystem
extends System

func on_process_entity(entity : AnimatedEntity, delta : float):
	
	var _animate = entity.get_component("animate") as AnimateComponent
	var _motion = entity.get_component("motion") as MotionComponent
	
	entity.animated_sprite.play(_animate.animation)
	entity.animated_sprite.flip_h = _animate.flip
