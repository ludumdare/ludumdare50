extends System

func on_process_entity(entity : Entity, delta : float):
	
	var _motion = entity.get_component("motion") as MotionComponent
	var _animate = entity.get_component("animate") as AnimateComponent

	
	if !_motion.bouncing:
		var _input = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	
#	if _motion.bounce == 0 and !_motion.bouncing:
		_motion.motion.x = _input * _motion.SPEED
		
	if _motion.bounce > 0:
		_motion.bounce -= 1
		_motion.motion.x = 1 * _motion.SPEED
		
	if _motion.bounce < 0:
		_motion.bounce += 1
		_motion.motion.x = -1 * _motion.SPEED
		
	
	if (_motion.motion.x < 0):
		_animate.flip = false
		
	if (_motion.motion.x > 0):
		_animate.flip = true
		
#	if (_motion.motion.x != 0) and entity.is_on_floor() and _motion.bounce != 0:
	if (_motion.motion.x != 0) and entity.is_on_floor() :
		_animate.animation = "run"
	else:
		if (entity.is_on_floor()):
			_animate.animation = "idle"	
			_motion.bouncing = false
			
			
	if (_motion.motion.y > 0):
		_animate.animation = "fall"	
