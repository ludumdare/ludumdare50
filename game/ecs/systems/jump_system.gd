class_name JumpSystem
extends System

func on_process_entity(entity : Entity, delta : float):
	
	var _jump = entity.get_component("jump") as JumpComponent
	var _motion = entity.get_component("motion") as MotionComponent
	var _animate = entity.get_component("animate") as AnimateComponent
	
	if Input.is_action_just_pressed("ui_up") and entity.is_on_floor():
		_motion.motion.y = - _jump.JUMP
		_animate.animation = "jump"
		_jump.DOUBLE = false
		
	if Input.is_action_just_pressed("ui_up") and !entity.is_on_floor():
		if (!_jump.DOUBLE):
			_jump.DOUBLE = true
			_motion.motion.y = - _jump.JUMP
			_animate.animation = "jump"
