class_name MotionSystem
extends System

func on_process_entity(entity : Entity, delta : float):
	
	var _motion = entity.get_component("motion") as MotionComponent
	var _animate = entity.get_component("animate") as AnimateComponent
	
	_motion.motion = entity.move_and_slide(_motion.motion, Vector2.UP)
	
	if (entity.is_on_floor() and _motion.bouncing):
		_motion.bouncing = false
		_animate.animation = "idle"	

	
		
