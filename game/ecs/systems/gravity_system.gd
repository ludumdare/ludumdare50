class_name GravitySystem
extends System

func on_process_entity(entity : Entity, delta : float):
	
	var _motion = entity.get_component("motion") as MotionComponent
	var _animate = entity.get_component("animate") as AnimateComponent
	
	_motion.motion.y += _motion.GRAVITY * delta
	
