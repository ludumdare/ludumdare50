extends System

func on_process_entity(entity : Entity, delta : float):
	
	var _collide = entity.get_component("collide") as CollideComponent
	
	if (_collide.collision_area == null):
		return
		
		
	if (_collide.has_collided and ("Death" in _collide.collision_area.name)):
		
		Logger.debug("Dead")
		
		# reset collision detector
		_collide.has_collided = false
	
		# remove entity
		ECS.remove_entity(entity)
	
